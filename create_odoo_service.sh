#!/bin/bash

# Generar nombres únicos para los contenedores y volúmenes
timestamp=$(date +%Y%m%d%H%M%S)
export ODOO_CONTAINER_NAME="odoo_15_${timestamp}"
export DB_CONTAINER_NAME="db_15_${timestamp}"
# export ODOO_WEB_DATA="odoo_web_data_${timestamp}"
# export ODOO_DB_DATA="odoo_db_data_${timestamp}"
export ODOO_PORT=$((8069 + $(date +%s) % 1000))  # Asigna un puerto aleatorio
project_name="odoo_project_${timestamp}"

# Ejecutar docker-compose con un nombre de proyecto único
docker-compose -p $project_name up -d
