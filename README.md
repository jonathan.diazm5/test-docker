# Odoo-Postgres-Docker

## Descripción del proyecto

El proyecto consta de un archivo docker-compose.yml que permite generar contenedores docker con instancias de odoo y con base de datos postgres respectivamente.
Para el despliegue de instancias de odoo, se deben modificar las variables de entorno dentro del archivo .env donde se especifican el nombre del contenedr de odoo, el nombre del contenedor de la base de datos, la versión de odoo y por último el puerto de comunicación del contenedor de odoo, para la comunicación con el host.

```
ODOO_CONTAINER_NAME=
ODOO_VERSION=
DB_CONTAINER_NAME=
ODOO_PORT=
```

### Estructura del proyecto

El proyecto está organizado de la siguiente manera

```
auth/
├── admin/
│   └── admin_token.txt/
│
└── app/
    ├── alembic/
    ├── config/
    ├── data/
    ├── middlewares/
    ├── models/
    ├── routers/
    ├── schemas/
    ├── services/
    ├── static/
    ├── utils/
    ├──  alembic.ini
    ├──  favicon.ico
    └──  main.py
```

El primer directorio que encontrarás es **admin** aquí encontrará el token administratico "admin_token.txt" una vez haya sido instalado el proyecto.

## 1. Auth API

La parte del API se encuentra en el directorio "app" y está estructurada de la siguiente manera:

- El directorio **alembic** contiene archivos automáticos de la herramienta alembic, como "script.py.mako", "
  README" y env.py. El directorio "versions" contiene archivos Python que gestionan la migración de las bases de datos.

- El directorio  **config** ccontiene un archivo ".env" que almacena las credenciales del servidor de base de datos.
  Además, contiene dos archivos de configuración de la conexión a la base de datos, "settings.py" y "database.py". El
  primero genera la URL de conexión a la base de datos y define algunas variables del globales, mientras que el segundo 
  configura la conexión a la base de datos. También se encuentra un archivo "auth_admin.py" que permite definir que endpoints
  solicitarán permisos administrativos.

- El directorio **data** contiene archivos .csv necesarios para cargar la base de datos durante la instalación, así como
  datos de prueba para algunos endpoints.

- En el directorio **middlewares** se encuentran módulos para aplicar características generales al API. Incluye "
  error_handler.py" para el manejo de errores, "jwt_bearer.py" que lee y gestiona los tokens de autenticación.

- El directorio <span style="color: blue;">**models**</span> contiene las definiciones de los modelos de la base de
  datos creados con SQLAlchemy.

- El directorio <span style="color: red;">**routers**</span> contiene varios módulos en donde se definen los endpoints
  de distintos tipos de peticiones tanto administrativas como de uso general.

- El directorio <span style="color: yellow;">**schemas**</span> contiene las definiciones de los modelos de Pydantic
  para validación en datos de entrada y salida de cada uno de los endpoints de la API.

- El directorio <span style="color: green;">**services**</span> contiene una serie de módulos en donde se define la
  lógica y procesamiento de la información implementada en cada uno de los endpoints.

Los anteriores cuatro directorios son el eje central del API.

- En el directorio **static** se encuentran algunos archivos de personalización visual de la documentación del API.

- El directorio **utils** contiene varios módulos necesarios para el propósito del API. *file_manager.py* contiene 
  funciones útiles para el procesamiento de información almacenada en archivos, *token_manager.py* permite
  gestionar la generación y verificación de tokens para la autenticación. Por último, *validations.py* contiene
  funciones de validación utilizadas en los modelos de validación Pydantic.

- Por último, el archivo ***main.py*** es el archivo principal en donde se integra todo; este es el que permite
  inicializar el API.

- Además de los anteriores módulos, el directorio **app** contiene algunos archivos de configuración general, como un
  icono *favicon.ico* y un archivo de configuración de alembic *alembic.ini* que nos permite gestionar las migraciones 
  de la base de datos en la instalación del API.

## Instalación del API

Para instalar el API, sigue los siguientes estos pasos:

### 1. Clonar el repositorio del proyecto con SSH

Ejecuta el siguiente comando en la terminal para clonar el repositorio del proyecto y luego ingresa al directorio del
proyecto

```shell
git clone git@gitlab.com:jorels/api/auth.git
cd nimbus/
```

### 2. Cambiar a la rama desarrollo

Asegúrate de estar en la rama de desarrollo con el siguiente comando:

```shell
git checkout dev
```

### 3. Crear un entorno virtual de python e instalar Poetry

Crea y activa el entorno de python con los comandos:

```shell
python3.10 -m venv venv
source /venv/bin/activate
```

Instala poetry con el siguiente comando:

```shell
pip install poetry
```

### 4. Instalación de dependencias con Poetry

Ejecuta el siguiente comando en la terminal para instalar las dependencias utilizando poetry:

```shell
poetry install --no-root
```

### 5. Instalación de postgres y configuración de la base de datos

Primero ejecuta los siguientes comandos para instalar el gestor de bases de datos postgres

```shell
sudo apt update
sudo apt install postgresql
```

Luego, accede a la base de datos como usuario "postgres" (asegurate de tener los permisos necesarios):

```shell
psql -d postgres
```

Elimina la base de datos llamada "auth_apy" si existe:

```shell
DROP DATABASE IF EXISTS nimbus;
```

Crea una base de datos llamada "auth_api":

```shell
CREATE DATABASE auth_api;
```

Por último, configura el usuario y la contraseña siguientes:

```shell
CREATE USER auth_api WITH PASSWORD 'auth_api';
```

### 6. Creación y carga de las tablas en la base de datos

**¡No inicializar el API antes de completar este paso para evitar conflictos durante el proceso de migración de datos en
la base de datos!**.

Dirígete al directorio "app":

```shell
cd app/
```

Ejecuta el comando de migración con Alembic:

```shell
alembic upgrade head
```

Una vez hayas completado estos pasos, habrás instalado el API y configurado la base de datos correctamente, así que
puedes ejecutar el API con el comando:

```shell
uvicorn main:app --host 127.0.0.1 --port 8000 --reload
```