# Usar la imagen oficial de Odoo como base
FROM odoo:latest

# Exponer los puertos necesarios
EXPOSE 8069 8071 8072

# Comando para iniciar Odoo
CMD ["odoo", "-d", "test_db_15",]